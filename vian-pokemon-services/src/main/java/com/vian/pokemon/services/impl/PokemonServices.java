package com.vian.pokemon.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vian.pokemon.exception.PokemonException;
import com.vian.pokemon.model.PokemonIdName;
import com.vian.pokemon.model.PokemonInfo;
import com.vian.pokemon.services.IPokemonServices;

import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResource;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResourceList;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;

public class PokemonServices implements IPokemonServices {

  private static final String ROCKET_TEAM_FRAUD = "Rocket team fraud";
  private static final int MAX_POKEMON_NUMBER = 1000;
  public static final String BAD_POKEMON_NAME = "Pokemon does not exists";

  private PokeApi pokeApi;

  /**
   * Calculate the minimum number of challenges to get the given tournament
   * results.
   * 
   * @param results Final pokemon ranking
   * @return Minimum number of challeges or too chaotic if not possible to get the
   *         given result
   */
  public String calcChallenges(List<String> results) {
    int[] finalOrder = new int[results.size()];
    int i = 0;
    HashMap<Integer, Integer> positionsByPokedex = new HashMap<>(finalOrder.length);
    HashMap<String, Integer> pokedex = this.getPokedexList();

    for (String pokemon : results) {
      finalOrder[i] = getPokedexByName(pokemon, pokedex);
      positionsByPokedex.put(finalOrder[i], i++);
    }

    int[] originalOrder = new int[results.size()];
    System.arraycopy(finalOrder, 0, originalOrder, 0, finalOrder.length);
    Arrays.sort(originalOrder);
    for (int j = 0; j < originalOrder.length; j++) {
      positionsByPokedex.put(originalOrder[j], j+1);
    }

    return calcNumberOfChallenges(positionsByPokedex, finalOrder);
  }

  private int getPokedexByName(String pokemon, HashMap<String, Integer> pokedex) {
    if (pokemon == null || "".equals(pokemon)) {
      throw new PokemonException(BAD_POKEMON_NAME);
    }

    String name = pokemon.toLowerCase();

    if (pokedex.containsKey(name)) {
      return pokedex.get(name);
    }

    throw new PokemonException(BAD_POKEMON_NAME + ": " + pokemon);
  }

  private String calcNumberOfChallenges(HashMap<Integer, Integer> positionsByPokedex, int[] finalOrder) {
    int ans = 0;

    for (int i = 0; i < finalOrder.length; i++) {
      if (positionsByPokedex.get(finalOrder[i]) - (i + 1) > 2) {
        return ROCKET_TEAM_FRAUD;
      }

      for (int j = Math.max(0, positionsByPokedex.get(finalOrder[i]) - 2); j < i; j++)
        if (finalOrder[j] > finalOrder[i])
          ans++;
    }

    return String.valueOf(ans);
  }

  /**
   * Get the pokedex list: pokemon and its index Has cache.
   */
  @Override
  public HashMap<String, Integer> getPokedexList() {
    PokeApi pokeApi = new PokeApiClient();
    NamedApiResourceList pokedexList = pokeApi.getPokemonList(0, MAX_POKEMON_NUMBER);
    List<NamedApiResource> results = pokedexList.getResults();

    HashMap<String, Integer> pokedex = new HashMap<>();

    for (NamedApiResource poke : results) {
      pokedex.put(poke.getName(), poke.getId());
    }

    return pokedex;
  }

  /**
   * Get pokemon info by its pokedex. Has cache.
   */
  @Override
  public PokemonInfo getPokemonInfo(int pokedex) {
    try {
      Pokemon pokemon = pokeApi.getPokemon(pokedex);
      return PokemonInfoFactory.createPokemonInfo(pokemon);
    } catch (Exception e) {
      throw new PokemonException(e);
    }
  }

  public void setpokeApi(PokeApi pokeApi) {
    this.pokeApi = pokeApi;
  }

  @Override
  public List<List<PokemonIdName>> getCalcChallengesSteps(List<String> results) {
    int[] finalOrder = new int[results.size()];
    int i = 0;
    HashMap<Integer, Integer> positionsByPokedex = new HashMap<>(finalOrder.length);
    HashMap<Integer, String> namesByPokedex = new HashMap<>(finalOrder.length);
    HashMap<String, Integer> pokedex = this.getPokedexList();

    for (String pokemon : results) {
      finalOrder[i] = getPokedexByName(pokemon, pokedex);
      namesByPokedex.put(finalOrder[i], pokemon);
      positionsByPokedex.put(finalOrder[i], i++);
    }

    return getCalcChallengesSteps(positionsByPokedex, namesByPokedex, finalOrder);
  }

  private List<List<PokemonIdName>> getCalcChallengesSteps(HashMap<Integer, Integer> positionsByPokedex,
      HashMap<Integer, String> namesByPokedex, int[] finalOrder) {
    List<List<PokemonIdName>> steps = new ArrayList<>();
    addStep(finalOrder, namesByPokedex, steps);
    int i = 0;

    while (i < finalOrder.length - 1) {
      if (positionsByPokedex.get(finalOrder[i]) - (i + 1) > 2) {
        steps.clear();
        steps.add(Arrays.asList(new PokemonIdName(0, ROCKET_TEAM_FRAUD)));
        return steps;
      }

      if (finalOrder[i] > finalOrder[i + 1]) {
        swap(finalOrder, i, i + 1);
        addStep(finalOrder, namesByPokedex, steps);
        i = Math.max(0, i - 1);
      } else {
        i++;
      }
    }

    return steps;
  }

  private void addStep(int[] finalOrder, HashMap<Integer, String> namesByPokedex, List<List<PokemonIdName>> steps) {
    List<PokemonIdName> ranking = new ArrayList<>();
    for (int j = 0; j < finalOrder.length; j++) {
      ranking.add(new PokemonIdName(finalOrder[j], namesByPokedex.get(finalOrder[j])));
    }

    steps.add(ranking);
  }

  private void swap(int[] finalOrder, int i, int j) {
    int tmp = finalOrder[i];
    finalOrder[i] = finalOrder[j];
    finalOrder[j] = tmp;
  }
}
