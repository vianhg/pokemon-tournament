package com.vian.pokemon.services;

import java.util.HashMap;
import java.util.List;

import com.vian.pokemon.model.PokemonIdName;
import com.vian.pokemon.model.PokemonInfo;
import com.vian.pokemon.model.TournamentResult;

import org.springframework.cache.annotation.Cacheable;

public interface IPokemonServices {

  String calcChallenges(List<String> results);

  List<List<PokemonIdName>> getCalcChallengesSteps(List<String> results);

  @Cacheable("pokedex")
  HashMap<String, Integer> getPokedexList();

  @Cacheable("pokedex")
  PokemonInfo getPokemonInfo(int pokedex);
}
