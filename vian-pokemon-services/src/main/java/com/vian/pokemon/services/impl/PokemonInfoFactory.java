package com.vian.pokemon.services.impl;

import java.util.stream.Collectors;

import com.vian.pokemon.model.PokemonInfo;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;

public class PokemonInfoFactory {

  public static PokemonInfo createPokemonInfo(Pokemon pokemon) {
    PokemonInfo pokemonInfo = new PokemonInfo();
    pokemonInfo
        .setAbilities(pokemon.getAbilities().stream().map(e -> e.getAbility().getName()).collect(Collectors.toList()));
    pokemonInfo.setBaseExperience(pokemon.getBaseExperience());
    pokemonInfo.setForms(pokemon.getForms().stream().map(e -> e.getName()).collect(Collectors.toList()));
    pokemonInfo.setHeight(pokemon.getHeight());
    pokemonInfo.setId(pokemon.getId());
    pokemonInfo.setImage(pokemon.getSprites().getFrontDefault());
    pokemonInfo.setMoves(pokemon.getMoves().stream().map(e -> e.getMove().getName()).collect(Collectors.toList()));
    pokemonInfo.setName(pokemon.getName());
    pokemonInfo.setOrder(pokemon.getOrder());
    pokemonInfo.setSpecies(pokemon.getSpecies().getName());
    pokemonInfo.setStats(pokemon.getStats().stream().map(e -> e.getStat().getName()).collect(Collectors.toList()));
    pokemonInfo.setTypes(pokemon.getTypes().stream().map(e -> e.getType().getName()).collect(Collectors.toList()));
    pokemonInfo.setWeight(pokemon.getWeight());
    return pokemonInfo;
  }

}
