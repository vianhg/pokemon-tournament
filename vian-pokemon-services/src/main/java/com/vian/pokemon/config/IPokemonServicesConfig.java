package com.vian.pokemon.config;

import com.vian.pokemon.services.IPokemonServices;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public interface IPokemonServicesConfig {

  @Bean
  IPokemonServices pokemonServices();

}