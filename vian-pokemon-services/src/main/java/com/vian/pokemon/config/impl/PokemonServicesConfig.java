package com.vian.pokemon.config.impl;

import java.util.Arrays;

import com.vian.pokemon.config.IPokemonServicesConfig;
import com.vian.pokemon.services.IPokemonServices;
import com.vian.pokemon.services.impl.PokemonServices;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;

@Configuration
@EnableCaching
public class PokemonServicesConfig implements IPokemonServicesConfig {

  @Bean
  public IPokemonServices pokemonServices() {
    PokemonServices pokemonServices = new PokemonServices();
    pokemonServices.setpokeApi(new PokeApiClient());
    return pokemonServices;
  }

  @Bean
  public CacheManager cacheManager() {
    SimpleCacheManager cacheManager = new SimpleCacheManager();
    cacheManager.setCaches(Arrays.asList(new ConcurrentMapCache("pokedex")));
    return cacheManager;
  }

}