package com.vian.pokemon.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vian.pokemon.config.impl.PokemonServicesConfig;
import com.vian.pokemon.model.PokemonIdName;
import com.vian.pokemon.model.PokemonInfo;
import com.vian.pokemon.services.IPokemonServices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import me.sargunvohra.lib.pokekotlin.client.PokeApi;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { PokemonServicesConfig.class })
public class PokemonServicesTests {

  @Autowired
  private IPokemonServices pokemonServices;

  @MockBean
  private PokeApi pokeApi;

  @Test
  public void givenPokedexWhenGetPokemonInfoThenReturnPokemonInfo() {
    PokemonInfo result = pokemonServices.getPokemonInfo(1);

    assertEquals("bulbasaur", result.getName());
  }

  @Test
  public void whenGetPokedexListThenReturnPokemonList() {
    HashMap<String, Integer> result = pokemonServices.getPokedexList();

    assertNotNull(result);
  }

  @Test
  public void givenTournamentResultWhenCalculateThenReturnMinChallenges() {
    List<String> tournamentResult = Arrays.asList("Squirtle", "Bulbasaur", "Charmander", "Caterpie", "Pidgey");
    String result = pokemonServices.calcChallenges(tournamentResult);
    System.err.println("result: " + result);
    assertEquals("2", result);
  }

  @Test
  public void givenFraudTournamentResultWhenCalculateThenReturnMinChallenges() {
    List<String> tournamentResult = Arrays.asList("Squirtle", "Pidgey", "Bulbasaur", "Charmander", "Caterpie");
    String result = pokemonServices.calcChallenges(tournamentResult);
    System.err.println("result: " + result);
    assertEquals("Rocket team fraud", result);
  }

  @Test
  public void givenTournamentResultWhenCalculateThenReturnMinChallenges0() {
    HashMap<Integer, Integer> positionsByPokedex = new HashMap<>();
    positionsByPokedex.put(1, 1);
    positionsByPokedex.put(4, 2);
    positionsByPokedex.put(7, 3);
    positionsByPokedex.put(10, 4);
    positionsByPokedex.put(16, 5);

    int[] finalOrder = { 7, 16, 1, 4, 10 };
    String result = calcNumberOfChallenges(positionsByPokedex, finalOrder);
    System.err.println(result);
    assertEquals("Rocket team fraud", result);
  }

  private String calcNumberOfChallenges(HashMap<Integer, Integer> positionsByPokedex, int[] finalOrder) {
    int ans = 0;
    for (int i = 0; i < finalOrder.length; i++) {
      if (positionsByPokedex.get(finalOrder[i]) - (i + 1) > 2) {
        return "Rocket team fraud";
      }

      for (int j = Math.max(0, positionsByPokedex.get(finalOrder[i]) - 2); j < i; j++)
        if (finalOrder[j] > finalOrder[i])
          ans++;
    }

    return String.valueOf(ans);
  }

  @Test
  public void givenTournamentResultWhenCalculateStepsThenReturnChallengesSteps() {
    List<String> tournamentResult = Arrays.asList("Squirtle", "Bulbasaur", "Charmander", "Caterpie", "Pidgey");

    List<List<PokemonIdName>> result = pokemonServices.getCalcChallengesSteps(tournamentResult);

    for (List<PokemonIdName> rank : result) {
      System.err.println(rank);
    }

    assertEquals(3, result.size());
  }

  @Test
  public void givenTournamentResultWhenCalculateStepsThenReturnChallengesSteps0() {
    HashMap<Integer, Integer> positionsByPokedex = new HashMap<>();
    positionsByPokedex.put(1, 1);
    positionsByPokedex.put(4, 2);
    positionsByPokedex.put(7, 3);
    positionsByPokedex.put(10, 4);
    positionsByPokedex.put(16, 5);

    int[] finalOrder = { 4, 7, 10, 1, 16 };
    List<List<PokemonIdName>> result = getCalcChallengesSteps(positionsByPokedex, finalOrder);

    for (List<PokemonIdName> rank : result) {
      System.err.println(rank);
    }
  }

  private List<List<PokemonIdName>> getCalcChallengesSteps(HashMap<Integer, Integer> positionsByPokedex,
      int[] finalOrder) {
    List<List<PokemonIdName>> steps = new ArrayList<>();
    addStep(finalOrder, steps);
    int i = 0;

    while (i < finalOrder.length - 1) {
      if (positionsByPokedex.get(finalOrder[i]) - (i + 1) > 2) {
        steps.clear();
        steps.add(Arrays.asList(new PokemonIdName(0, "Rocket team fraud" + i)));
        return steps;
      }

      if (finalOrder[i] > finalOrder[i + 1]) {
        swap(finalOrder, i, i + 1);
        addStep(finalOrder, steps);
        i = Math.max(0, i - 1);
      } else {
        i++;
      }
    }

    return steps;
  }

  private void addStep(int[] finalOrder, List<List<PokemonIdName>> steps) {
    List<PokemonIdName> ranking = new ArrayList<>();
    for (int j = 0; j < finalOrder.length; j++) {
      ranking.add(new PokemonIdName(finalOrder[j], "ab"));
    }

    steps.add(ranking);
  }

  private void swap(int[] finalOrder, int i, int j) {
    int tmp = finalOrder[i];
    finalOrder[i] = finalOrder[j];
    finalOrder[j] = tmp;
  }

  @Test
  public void givenBigTournamentResultWithNoChangesWhenCalculateThenReturnMinChallenges()
      throws JsonParseException, JsonMappingException, IOException {

    String result = calcWithBigTournament("pokemon-test0.json");
    assertEquals("0", result);
  }

  @Test
  public void givenBigTournamentResultWithFewChangesWhenCalculateThenReturnMinChallenges()
      throws JsonParseException, JsonMappingException, IOException {

    String result = calcWithBigTournament("pokemon-test1.json");
    assertEquals("6", result);
  }

  @Test
  public void givenBigTournamentResultWithManyChangesWhenCalculateThenReturnMinChallenges()
      throws JsonParseException, JsonMappingException, IOException {

    String result = calcWithBigTournament("pokemon-test2.json");
    assertEquals("Rocket team fraud", result);
  }

  private String calcWithBigTournament(String fileName) throws IOException, JsonParseException, JsonMappingException {
    long d1 = System.currentTimeMillis();
    ObjectMapper objectMapper = new ObjectMapper();
    List<String> tournamentResult = objectMapper.readValue(new File("src/test/resources/" + fileName), List.class);

    String result = pokemonServices.calcChallenges(tournamentResult);

    System.err.println("Time ms:" + (d1 - System.currentTimeMillis()));
    System.err.println("result: " + result);
    return result;
  }
}
