package com.vian.pokemon.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.vian.pokemon.config.impl.PokemonAPIConfig;
import com.vian.pokemon.model.PokemonInfo;
import com.vian.pokemon.model.TournamentResult;
import com.vian.pokemon.services.IPokemonServices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { PokemonAPIConfig.class })
public class PokemonControllerTests {

  @Autowired
  private IPokemonController pokemonController;

  @MockBean
  private IPokemonServices pokemonServices;

  @Test
  public void whenGetPokedexThenReturnAllPokemonList() {
    HashMap<String, Integer> pokedex = new HashMap<>();
    Integer index = 1;
    String name = "bulbasaur1";
    pokedex.put(name, index);
    
    given(pokemonServices.getPokedexList()).willReturn(pokedex);
    
    assertEquals(pokedex.size(), pokemonController.getPokedexList().size());
    assertEquals(index, pokemonController.getPokedexList().get(name));
  }

  @Test
  public void givenPokedexwhenGetPokemonThenReturnPokemonInfo() {
    PokemonInfo pokemon = new PokemonInfo();
    pokemon.setName("bulbasaur1");
    pokemon.setId(1);
    
    given(pokemonServices.getPokemonInfo(1)).willReturn(pokemon);
    PokemonInfo result = pokemonController.getPokemonInfo(1);
    
    assertEquals(pokemon.getName(), result.getName());
    assertEquals(pokemon.getId(), result.getId());
  }

  @Test
  public void givenTournamentResultWhenCalculateThenReturnMinChallenges() {
    List<String> tournamentResult = Arrays.asList("Squirtle", "Bulbasaur", "Charmander", "Caterpie", "Pidgey");
    given(pokemonServices.calcChallenges(tournamentResult)).willReturn("2");

    String result = pokemonController.calculateChallenges(tournamentResult).getBody();
    
    assertEquals("2", result);
  }
}
