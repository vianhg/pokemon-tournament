package com.vian.pokemon.config.impl;

import com.vian.pokemon.api.IPokemonController;
import com.vian.pokemon.api.impl.PokemonController;
import com.vian.pokemon.config.IPokemonAPIConfig;
import com.vian.pokemon.services.IPokemonServices;

import org.springframework.context.annotation.Import;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Import({ PokemonServicesConfig.class })
public class PokemonAPIConfig implements IPokemonAPIConfig {

  @Autowired
  private IPokemonServices pokemonServices;

  @Override
  @Bean
  public IPokemonController pokemonController() {
    PokemonController pokemonController = new PokemonController();
    pokemonController.setPokemonServices(pokemonServices);
    return pokemonController;
  }

  /*@Bean
  public CacheManager cacheManager() {
    SimpleCacheManager cacheManager = new SimpleCacheManager();
    cacheManager.setCaches(Arrays.asList(new ConcurrentMapCache("pokedex")));
    return cacheManager;
  }*/

}
