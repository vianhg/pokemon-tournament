package com.vian.pokemon.config;

import com.vian.pokemon.api.IPokemonController;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public interface IPokemonAPIConfig {

  @Bean
  IPokemonController pokemonController();

}