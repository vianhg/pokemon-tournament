package com.vian.pokemon.api.impl;

import java.util.HashMap;
import java.util.List;

import com.vian.pokemon.api.IPokemonController;
import com.vian.pokemon.exception.PokemonException;
import com.vian.pokemon.model.PokemonIdName;
import com.vian.pokemon.model.PokemonInfo;
import com.vian.pokemon.model.TournamentResult;
import com.vian.pokemon.services.IPokemonServices;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class PokemonController implements IPokemonController {

  private IPokemonServices pokemonServices;

  public ResponseEntity<String> calculateChallenges(List<String> tournamentResults) {
    try {
      String ans = pokemonServices.calcChallenges(tournamentResults);
      return ResponseEntity.ok(ans);
    } catch (PokemonException e) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.toString());
    }
  }

  public ResponseEntity<List<List<PokemonIdName>>> getCalcChallengesSteps(List<String> tournamentResults) {
    try {
      List<List<PokemonIdName>> ans = pokemonServices.getCalcChallengesSteps(tournamentResults);
      return ResponseEntity.ok(ans);
    } catch (PokemonException e) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  public HashMap<String, Integer> getPokedexList() {
    return pokemonServices.getPokedexList();
  }

  public PokemonInfo getPokemonInfo(int pokedex) {
    return pokemonServices.getPokemonInfo(pokedex);
  }

  public void setPokemonServices(IPokemonServices pokemonServices) {
    this.pokemonServices = pokemonServices;
  }

}