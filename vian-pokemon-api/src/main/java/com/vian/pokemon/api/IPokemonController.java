package com.vian.pokemon.api;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import com.vian.pokemon.model.PokemonIdName;
import com.vian.pokemon.model.PokemonInfo;
import com.vian.pokemon.model.TournamentResult;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public interface IPokemonController {

  @PostMapping(path = "/challenges", consumes = "application/json", produces = "text/plain")
  ResponseEntity<String> calculateChallenges(@Valid @RequestBody List<String> tournamentResults);

  @GetMapping("/pokedex")
  HashMap<String, Integer> getPokedexList();

  @GetMapping("/pokemon")
  PokemonInfo getPokemonInfo(@RequestParam int pokedex);

  @PostMapping(path = "/steps", consumes = "application/json", produces = "application/json")
  ResponseEntity<List<List<PokemonIdName>>> getCalcChallengesSteps(@Valid @RequestBody List<String> tournamentResults);
}
