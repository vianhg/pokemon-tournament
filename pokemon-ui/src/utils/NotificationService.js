import { ToastProvider, useToasts } from 'react-toast-notifications'

export const notificationService = {

  showError(msg = "There was an error on the request. Please check if the host is ok.") {
    alert(msg);
  //   const { addToast } = useToasts();
  //   addToast(msg, { appearance: 'error', autoDismiss: true });
  }

}

export default notificationService;