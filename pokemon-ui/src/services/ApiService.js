import { API_URL } from "./config";
import notificationService from '../utils/NotificationService';

export const ApiService = {
  get(resource) {
    return fetch(API_URL + resource)
      .then(res => res.json())
      .catch(error => {
        console.error(error);
        notificationService.showError();
        throw new Error(`[Pokemon Exception] when getting API ${error}`);
      });
  },
  post(resource, data) {
    return fetch(API_URL + resource, {
      method: "POST",
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: data
    }).then(res => res.text())
      .catch(error => {
        console.error(error);
        notificationService.showError();
        throw new Error(`[Pokemon Exception] when getting API ${error}`);
      });
  }
};

export default ApiService;
