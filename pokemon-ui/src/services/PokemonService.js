import ApiService from "./ApiService";
import notificationService from '../utils/NotificationService';

export const PokemonService = {
  getPokedex() {
    return ApiService.get("pokedex");
  },
  getPokemon(pokedex) {
    if (isNaN(pokedex)) {
      notificationService.showError("The given pokedex is not right.");
      throw new Error("[Pokemon error] the given pokedex is not right." + pokedex);
    }
    return ApiService.get(`pokemon?pokedex=${pokedex}`);
  },
  calcMinChallenges(tournamentResults) {
    if (!tournamentResults) {
      notificationService.showError("The given JSON with tournament results is not right.");
      throw new Error("[Pokemon error] the given JSON with tournament results is not right.");
    }
    return ApiService.post("challenges", tournamentResults);
  },
  getSteps(tournamentResults) {
    if (!tournamentResults) {
      notificationService.showError("The given JSON with tournament results is not right.");
      throw new Error("[Pokemon error] the given JSON with tournament results is not right.");
    }
    return ApiService.post("steps", tournamentResults);
  }
};

export default PokemonService;
