import React from 'react';
import PokemonLevelComponent from './PokemonLevelComponent'
import PokemonService from '../services/PokemonService';
import notificationService from '../utils/NotificationService';
import {Col} from 'reactstrap';
import SPRITE_POKEMON_BASE_URL from '../constants/constants';

class TournamentChallengesComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { steps: [] };
  }

  componentWillMount() {
    const { id } = this.props.match.params
    PokemonService.getSteps(id)
      .then((response) => {
        this.setState({ steps: response })
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <div className="container">
        <PokemonSteps steps={this.state.steps}></PokemonSteps>
      </div>
    )
  }
}

const PokemonSteps = (s) => {
  const steps = s.steps;
  if (steps.length === 0) {
    return <div></div>;
  }

  return (
    <div>
      {
        steps.map((ranking, idx) => {
          return (
            <PokemonStep
              key={idx}
              ranking={ranking}>
            </PokemonStep>
          );
        })
      }
    </div>
  )
}

const PokemonStep = (ranking) => {
  if (ranking.length === 0) {
    return <div></div>;
  }

  return (
    <Col>
      {
        ranking.map((pokemon, idx) => {
          return (
            <PokemonLevelComponent
              key={idx}
              pokedex={pokemon.id}
              name={pokemon.name}
              image={SPRITE_POKEMON_BASE_URL + pokemon.id + ".png"}>
            </PokemonLevelComponent>
          );
        })
      }
    </Col>
  )
}

export default TournamentChallengesComponent;