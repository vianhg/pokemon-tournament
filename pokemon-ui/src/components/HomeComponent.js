import React from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardBody,
  Row,
  Col,
  CardTitle
} from "reactstrap";

class HomeComponent extends React.Component {
  render() {
    return (
      <div className="Home">
        <div className="container">
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <CardTitle>
                    <Link to="/pokedex" className="card-link text-center">
                      <div>
                        <img alt="" src={require("../assets/images/pokedex.jpg")} className="logo-image" />
                      </div>
                      <h3>Pokedex</h3>
                    </Link>
                  </CardTitle>
                </CardBody>
              </Card>
            </Col>
            <Col>
              <Card>
                <CardBody>
                  <CardTitle>
                    <Link to="/tournament" className="card-link text-center">
                      <div>
                        <img alt="" src={require("../assets/images/tournament.jpg")} className="logo-image" />
                      </div>
                      <h3>Tournament</h3>
                    </Link>
                  </CardTitle>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div >
    )
  }
}

export default HomeComponent;