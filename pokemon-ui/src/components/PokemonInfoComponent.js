import React from 'react';
import './PokemonInfoComponent.css';
import { Link } from 'react-router-dom';
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Button,
  UncontrolledCollapse
} from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleLeft, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import notificationService from '../utils/NotificationService';

import PokemonService from '../services/PokemonService'

// const POKEMON_INFO_LOADED = "POKEMON_INFO_LOADED";
// const mapDispatchToProps = dispatch => ({
//   onLoad: payload => dispatch({ type: POKEMON_INFO_LOADED, payload })
// });

class PokemonInfoComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { pokemon: {} };
  }

  componentWillMount() {
    const { id } = this.props.match.params
    PokemonService.getPokemon(id)
      .then((response) => {
        this.setState({ pokemon: response })
      })
      .catch(error => {
        notificationService.showError();
      });
  }

  joinList(list) {
    if (list) {
      return list.join(", ");
    }
    return "";
  }

  render() {
    const pokemon = this.state.pokemon;
    const types = this.joinList(pokemon.types);
    const abilities = this.joinList(pokemon.abilities);
    const forms = this.joinList(pokemon.forms);
    const moves = this.joinList(pokemon.moves);

    return (
      <div className="container mb-5 ">
        <Card className="card-pokemon-info bg-light shadow">
          <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0">
            <img top="true" width="150px" className="rounded-circle text-center pokemon-img"
              src={pokemon.image}
              alt="Pokemon"
            />
            <div className="characteristics">
              <Row>
                <Col>
                  <div className="heading">{pokemon.order}</div>
                  <div className="description">Order</div>
                </Col>
                <Col>
                  <div className="heading">{pokemon.weight}</div>
                  <div className="description">Weight</div>
                </Col>
                <Col>
                  <div className="heading">{pokemon.height}</div>
                  <div className="description">Height</div>
                </Col>
              </Row>
              <div className="text-center">
                <h3 className="text-capitalize">{pokemon.name}</h3>
                <div className="h5">
                  Species: {pokemon.species}
                </div>
              </div>
            </div>
          </CardHeader>
          <CardBody className="pt-0 pt-md-1">
            <div className="heading-characteristics-body">Type</div>
            <div >{types}</div>
            <div className="heading-characteristics-body">Abilities</div>
            <div >{abilities}</div>
            <div className="text-center">
              <Button color="link" id="toggler">
                <FontAwesomeIcon icon={faCaretDown} size="2x" />
              </Button>
            </div>
            <UncontrolledCollapse toggler="#toggler">
              <div className="heading-characteristics-body">Forms</div>
              <div >{forms}</div>
              <div className="heading-characteristics-body">Moves</div>
              <div >{moves}</div>
            </UncontrolledCollapse>
          </CardBody>
        </Card>
        <Link to="/pokedex">
          <Button color="link" className="float-right mt-2 text-warning" title="Go back to pokedex">
            <FontAwesomeIcon icon={faArrowAltCircleLeft} size="3x" />
          </Button></Link>
      </div >
    )
  }
}

export default PokemonInfoComponent;
