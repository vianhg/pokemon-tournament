import React from 'react';

class HeaderComponent extends React.Component {
  render() {
    return (
        <div className="header bg-gradient-info pb-8 pt-5 pt-md-8">
          <div className="mb-0 ml-4 text-center">
            <h1>Pokemon Arena League</h1>
            <hr className="mt-4" />
          </div>
        </div>
    )
  }
}

export default HeaderComponent;