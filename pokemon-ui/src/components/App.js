import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import HomeComponent from './HomeComponent';
import PokedexComponent from './PokedexComponent';
import PokemonInfoComponent from './PokemonInfoComponent';
import TournamentComponent from './TournamentComponent';
import TournamentChallengesComponent from './TournamentChallengesComponent'
import NotFoundComponent from './NotFoundComponent';
import AppNavbar from './AppNavbar';
import HeaderComponent from './HeaderComponent';

function App() {
  return (
    <div className="App">
      <AppNavbar></AppNavbar>
      <HeaderComponent></HeaderComponent>
      <Switch>
        <Route exact path="/" component={HomeComponent} />
        <Route path="/tournament" component={TournamentComponent} />
        <Route exact path="/pokedex" component={PokedexComponent} />
        <Route path="/pokedex/:id" component={PokemonInfoComponent} /> pokedex
        <Route path="/challenges" component={TournamentChallengesComponent} />
        <Route path="*" component={NotFoundComponent} />
      </Switch>
    </div>
  );
}

export default App;
