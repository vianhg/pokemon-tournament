import React from 'react';
import './PokedexComponent.css';
import PokedexItemComponent from './PokedexItemComponent';
import notificationService from '../utils/NotificationService';
import PokemonService from '../services/PokemonService';
import SPRITE_POKEMON_BASE_URL from '../constants/constants';


class PokedexComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { pokedex: {} };
  }

  componentDidMount() {
    PokemonService.getPokedex()
      .then((response) => {
        this.setState({ pokedex: response })
      })
      .catch(error => {
        notificationService.showError();
      });
  }

  render() {
    const pokedex = this.state.pokedex;

    return (
      <div className="container pokedexBox mt-5">
        <PokemonList pokedex={pokedex}></PokemonList>
      </div>
    )
  }
}

const PokemonList = (p) => {
  const pokedex = p.pokedex;
  let pokemonList = Object.keys(pokedex).sort();
  if (pokemonList.length === 0) {
    return <div></div>;
  }

  return (
    <div className="container pokedexBox mt-5">
      {
        pokemonList.map((pokemon) => {
          return (
            <PokedexItemComponent
              key={pokedex[pokemon]}
              id={pokedex[pokemon]}
              name={pokemon}
              imagen={SPRITE_POKEMON_BASE_URL + pokedex[pokemon] + ".png"}>
            </PokedexItemComponent>
          );
        })
      }
    </div>
  )
}

export default PokedexComponent;