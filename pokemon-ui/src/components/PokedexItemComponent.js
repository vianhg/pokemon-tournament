import React from 'react';
import './PokedexComponent.css';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazy-load';

class PokedexItemComponent extends React.Component {
  render() {
    return (
      <div className="pokedexItem text-center">
        <LazyLoad debounce={false} throttle={500}>
          <Link to={"/pokedex/" + this.props.id} className="thumbnail">
            <img src={this.props.imagen} alt=" " />
            <span>{this.props.name}</span>
          </Link>
        </LazyLoad>
      </div>
    )
  }
}

export default PokedexItemComponent;