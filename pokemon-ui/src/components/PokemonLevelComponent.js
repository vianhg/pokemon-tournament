import React from 'react';
import { Card, CardBody, CardTitle, Row, Col } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleUp } from '@fortawesome/free-solid-svg-icons'

class PokemonLevelComponent extends React.Component {
  render() {
    return (
      <Card className="card-pokedex mb-4 mb-xl-2">
        <CardBody className="pt-0 pb-0">
          <Row>
            <Col xs="2">
              <img height="90%" className="rounded-circle" src={this.props.image} alt=" " />
            </Col>
            <Col xs="9">
              <CardTitle
                className="text-uppercase"
              >
                <div className="mt-30px">
                  <h5><span className="text-muted font-weight-light">
                    [{this.props.pokedex}]</span> {this.props.name}
                  </h5>
                </div>
              </CardTitle>
            </Col>
          </Row>
        </CardBody>
      </Card>
    )
  }
}

export default PokemonLevelComponent;

/*
<Col xs="1">
              <div className="text-success mt-30px">
                <FontAwesomeIcon icon={faArrowCircleUp} size="lg"/>
              </div>
            </Col>*/