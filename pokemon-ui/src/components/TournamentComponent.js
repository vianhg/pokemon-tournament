import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Label
} from "reactstrap";
import notificationService from '../utils/NotificationService';
import PokemonService from '../services/PokemonService';

class TournamentComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tournamentResults: '', minimumChalenges: null };

    this.handleChange = this.handleChange.bind(this);
    this.handleCompute = this.handleCompute.bind(this);
    this.handleShowchallenges = this.handleShowchallenges.bind(this);
  }

  handleChange(event) {
    this.setState({ tournamentResults: event.target.value, minimumChalenges: null });
  }

  isJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  handleCompute(event) {
    const results = this.state.tournamentResults;
    if (!results || !this.isJsonString(results)) {
      notificationService.showError("Please enter a valid JSON");
      return;
    }

    PokemonService.calcMinChallenges(results).then((response) => {
      this.setState({ minimumChalenges: response })
    }).catch(error => {
      console.error(error);
    });;
  }

  handleShowchallenges(event) {
    let { history } = this.props;
    history.push('/challenges');
    event.preventDefault();
  }


  render() {
    return (
      <div className="container">
        <Card className="shadow border-0">
          <CardHeader className="bg-transparent pb-5">
            <h3 className="text-center mt-2 mb-3">
              Tournament Challenges
            </h3>
            <div className="btn-wrapper text-center" />
          </CardHeader>
          <CardBody className="px-lg-5 py-lg-5">
            <Form role="form">
              <FormGroup className="mb-3">
                <Label for="txtRanking">Enter final result JSON</Label>
                <Input
                  id="txtRanking"
                  className="form-control-alternative"
                  placeholder={"[\"bulbasaur\", \"charmander\", \"squirtle\", \"caterpie\"]"}
                  rows="3"
                  type="textarea"
                  value={this.state.tournamentResults}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <div className="text-center">
                <Button className="my-4" color="primary" type="button" onClick={this.handleCompute}>
                  Compute
                </Button>
              </div>
              <MinimumChalenges minimumChalenges={this.state.minimumChalenges}></MinimumChalenges>
            </Form>
          </CardBody>
        </Card>
      </div>
    )
  }
}

const MinimumChalenges = (props) => {
  if (typeof props.minimumChalenges === "object" || props.minimumChalenges === null) {
    console.log(props.minimumChalenges);
    return "";
  }

  if (isNaN(props.minimumChalenges)) {
    return (
      <div className="text-center h5">
        It's not possible. Must be a fraud of the Rocket team!
      </div>
    );  
  }

  return (
    <div className="text-center h5">
      It's required at least <span className="badge badge-warning mr-1">
        {props.minimumChalenges} </span>
      challenges to get this result.
    </div>
  );
};

export default withRouter(TournamentComponent);

/*<div className="text-center">
<Button color="link" type="button" onClick={this.handleShowchallenges}>
  Show challenges
  </Button>
</div>*/