import React from "react";
import { Link } from "react-router-dom";

import {
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  NavItem,
  NavLink,
  NavbarBrand,
  UncontrolledCollapse
} from "reactstrap";

class AppNavbar extends React.Component {
  render() {
    return (
      <div>
        <Navbar className="navbar-top navbar-dark bg-dark" expand="md" id="navbar-main">
          <Container fluid>
            <NavbarBrand to="/" tag={Link}>
              <img alt="" src={require("../assets/images/logo.svg")} className="logoIcon" />
              Pokemon Arena
            </NavbarBrand>
            <button className="navbar-toggler" id="navbar-collapse-main">
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar-collapse-main">
              <div className="navbar-collapse-header d-md-none">
                
              </div>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink
                    className="nav-link-icon"
                    to="/pokedex"
                    tag={Link}
                  >
                    <i className="ni ni-circle-08" />
                    <span className="nav-link-inner--text">Pokedex</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className="nav-link-icon"
                    to="/tournament"
                    tag={Link}
                  >
                    <i className="ni ni-key-25" />
                    <span className="nav-link-inner--text">Tournament results</span>
                  </NavLink>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

export default AppNavbar;
