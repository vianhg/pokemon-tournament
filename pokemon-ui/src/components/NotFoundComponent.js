import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundComponent = () => (
    <div>
        <h3>404 - Not found</h3>
        <p>We can't seem to find the page you're looking for.</p>
        <center><Link to="/">Return to Home Page</Link></center>
    </div>
);

export default NotFoundComponent;