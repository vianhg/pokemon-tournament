package com.vian.pokemon.model;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TournamentResult  implements Serializable {

  private static final long serialVersionUID = 3900419749228130333L;

  private List<String> results;
  
  public List<String> getResults() {
    return results;
  }

  public void setResults(List<String> result) {
    results = result;
  }
}