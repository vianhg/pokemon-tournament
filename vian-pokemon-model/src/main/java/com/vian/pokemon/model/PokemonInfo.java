package com.vian.pokemon.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonInfo implements Serializable {

  private static final long serialVersionUID = -8150377394003065112L;
  private int id;
  private String name;
  private int baseExperience;
  private int height;
  private int order;
  private int weight;
  private String species;
  private List<String> abilities;
  private List<String> forms;
  private List<String> moves;
  private List<String> stats;
  private List<String> types;
  private String image;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getBaseExperience() {
    return baseExperience;
  }

  public void setBaseExperience(int baseExperience) {
    this.baseExperience = baseExperience;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public String getSpecies() {
    return species;
  }

  public void setSpecies(String species) {
    this.species = species;
  }

  public List<String> getAbilities() {
    return abilities;
  }

  public void setAbilities(List<String> abilities) {
    this.abilities = abilities;
  }

  public List<String> getForms() {
    return forms;
  }

  public void setForms(List<String> forms) {
    this.forms = forms;
  }

  public List<String> getMoves() {
    return moves;
  }

  public void setMoves(List<String> moves) {
    this.moves = moves;
  }

  public List<String> getStats() {
    return stats;
  }

  public void setStats(List<String> stats) {
    this.stats = stats;
  }

  public List<String> getTypes() {
    return types;
  }

  public void setTypes(List<String> types) {
    this.types = types;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}
