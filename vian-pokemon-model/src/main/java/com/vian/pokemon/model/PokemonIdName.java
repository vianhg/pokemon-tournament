package com.vian.pokemon.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonIdName implements Serializable {

  private static final long serialVersionUID = -8150377394003065112L;
  private int id;
  private String name;

  public PokemonIdName(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String toString() {
    return "[" + id + "," + name + "]";
  }
}
