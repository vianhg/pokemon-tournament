Pokémon league Arena!

There is a war in Pallet town. Every Pokémon is fighting for its honor and a place on the world’s Pokémon ranking. At the beginning, every Pokémon starts with a default ranking given by the order of appearance on the national pokédex.

The battle rules are easy:
· Each Pokémon may only fight with its very next upper raking brother.
· The first Pokémon on the list won’t be able to challenge anyone.
· Each Pokémon may fight only twice.

Take into account, that the team rocket disrupted the last Pokémon event. Jessie, James and Meouth himself tried to cease and capture all pokémons with their monstrous machines. Fortunately, Nurse Joy was present. She restored all the pokémons health. At the end, nobody was hurt. However, the most important gym leaders decided that this year the battles would be private. Only the result of the battle will be publish on the mayor pallet town’s newspapers.

Your first task is to read the Pokémon fight  results on the newspaper.
Then, order them according to the pokédex appearance. For example:

Id Description
1 Bulbasaur
4 Charmander
7 Squirtle
10 Caterpie
16 Pidgey

Your next task is to calculate the minimum fights that took place, in order to lead to the given result.
For example: For the next result, you could assume that there were 2 fights. Squirtle challenged Charmander and then Bulbasaur. For this result, your calculation must return: .2.

In addition, we heard that the team rocket might still be active. One of their team members could manipulate the results of the fights. If your calculations find that the result is not possible according to the rules, it must warn the user about it.

Finally, there is an open spot for gym leader. In order to apply for this position, you must provide a user interface for the public, so they can see all the battle results and check all the pokémons info.

Tips:
· You must use the services published on the pokeapi web page in order to get the information about the pokémons. 
· Keep in mind that the “fair use policy” is mandatory on the exercise implementation, in order to avoid being banned. Be warned that the open IP may not be used for this exercise


