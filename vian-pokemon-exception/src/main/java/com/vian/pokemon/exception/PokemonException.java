package com.vian.pokemon.exception;

public class PokemonException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public PokemonException() {
    super();
  }

  public PokemonException(String s) {
    super(s);
  }

  public PokemonException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public PokemonException(Throwable throwable) {
    super(throwable);
  }

}
